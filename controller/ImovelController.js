const ImovelService = require("../service/ImovelService");

class ImovelController {
  constructor() {
    this.ImovelService = new ImovelService();
  }

  async createImovel(req, res) {
    const data = {
      ...req.body,
    };
    try {
      await this.ImovelService.createImovel(data);
      res.status(204).json();
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async deleteImovel(req, res) {
    const { code } = req.params;
    try {
      await this.ImovelService.deleteImovel(code);
      res.status(204).json();
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async getAllImovel(req, res) {
    try {
      const result = await this.ImovelService.getAllImovel();
      res.status(200).json(result);
      return;
    } catch (error) {
      res.status(400).json({ error: error.message });
      return;
    }
  }

  async getOneImovel(req, res) {
    const { code } = req.params;
    try {
      const result = await this.ImovelService.getOneImovel(code);
      res.status(200).json(result);
      return;
    } catch (error) {
      res.status(400).json({ error: error.message });
      return;
    }
  }

  async updateImovel(req, res) {
    const data = {
      ...req.body,
    };
    try {
      const result = await this.ImovelService.updateImovel(data);
      res.status(200).json(result);
      return;
    } catch (error) {
      res.status(400).json({ error: error.message });
      return;
    }
  }
}

module.exports = ImovelController;
