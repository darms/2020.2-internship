const ImobiliariaService = require("../service/ImobiliariaService");

class ImobiliariaController {
  constructor() {
    this.ImobiliariaService = new ImobiliariaService();
  }

  async createImobiliaria(req, res) {
    const data = {
      ...req.body,
    };
    try {
      await this.ImobiliariaService.createImobiliaria(data);
      res.status(204).json();
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async deleteImobiliaria(req, res) {
    const { cnpj } = req.params;
    try {
      await this.ImobiliariaService.deleteImobiliaria(cnpj);
      res.status(204).json();
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  }

  async getAllImobiliaria(req, res) {
    try {
      const result = await this.ImobiliariaService.getAllImobiliaria();
      res.status(200).json(result);
      return;
    } catch (error) {
      res.status(400).json({ error: error.message });
      return;
    }
  }

  async getOneImobiliaria(req, res) {
    const {cnpj} = req.params;
    try {
      const result = await this.ImobiliariaService.getOneImobiliaria(cnpj);
      res.status(200).json(result);
      return;
    } catch (error) {
      res.status(400).json({ error: error.message });
      return;
    }
  }

  async updateImobiliaria(req, res) {
    const data = {
      ...req.body,
    };
    try {
      const result = await this.ImobiliariaService.updateImobiliaria(data);
      res.status(200).json(result);
      return;
    } catch (error) {
      console.log(error);
      res.status(400).json({ error: error.message });
      return;
    }
  }
};

module.exports = ImobiliariaController;