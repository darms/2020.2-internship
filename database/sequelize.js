const Sequelize = require('sequelize');
const databaseConfig = require('./config');

const nodeEnv = process.env.NODE_ENV || 'development';

const sequelize = new Sequelize(databaseConfig[nodeEnv]);

module.exports = sequelize;
