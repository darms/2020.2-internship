const imoveis = require("../models/imovel");
const imobiliaria = require("../models/imobiliaria");
const ImobiliariaService = require("./ImobiliariaService");
const sequelize = require("../database/sequelize");

class ImovelService {
    constructor(){
        this.ImobiliariaService = new ImobiliariaService();
    }

  async createImovel(data) {
    const validCNPJ = await this.ImobiliariaService.getOneImobiliaria(data.imobiliariaCNPJ);
    
    if(validCNPJ!=null){
       await imoveis.sync().then(function () {
         return imoveis
           .create({
             code: data.code,
             category: data.category,
             roomQuantity: data.roomQuantity,
             imobiliariaCNPJ: data.imobiliariaCNPJ,
           })
           .catch((error) => {
             throw new Error("Não foi possível criar o seu imovel");
           });
       });
    } else {
      throw new Error("O imobiliariaCNPJ não é válido");
    }
   
    return;
  }

  async getAllImovel() {
    const result = await imoveis
      .findAll({
        order: [
          ['roomQuantity', 'ASC'],
        ],
        include:[
          {model: imobiliaria, as:'Imobiliaria'}
        ]})
      .then((res) => {
        return res;
      });

    return result;
  }

  async getOneImovel(code) {
    const result = await imoveis.findOne({
      where: {
        code: code,
      },
      include: [{ model: imobiliaria, as: "Imobiliaria" }],
    });
    if(result==null){
      throw new Error('Não foi encontrado o imovel desejado.');
    }
    return result;
  }

  async deleteImovel(code) {
    await imoveis.destroy({
      where: {
        code: code,
      },
    });
    return;
  }

  async updateImovel(data) {
    const code = data.newCode;
    const category = data.category;
    const roomQuantity = data.roomQuantity;
    const isRented = data.isRented;
    const isPublished = data.isPublished;

    let validBody = 0;
    const dataUpdate = {};
    
    if (code != null) {
      validBody = 1;
      dataUpdate.code = code;
    }

    if (category != null) {
      validBody = 1;
      dataUpdate.category = category;
    }

    if (roomQuantity != null) {
      validBody = 1;
      dataUpdate.roomQuantity = roomQuantity;
    }

    if (isRented != null) {
      validBody = 1;
      dataUpdate.isRented = isRented;
    }

    if (isPublished != null) {
      validBody = 1;
      dataUpdate.isPublished = isPublished;
    }
    
    if (isRented == true) {
      dataUpdate.isPublished = false;
    }


    if (!validBody) {
      var errorMessage =
        'O body da requisição está incorreto. ';
      throw new Error(errorMessage);
    }
    const result = await imoveis.update(dataUpdate, {
      returning: true,
      where: {
        code: data.oldCode,
      },
    });
    
    if(result[1][0] == undefined) {
      throw new Error("O imovel não foi encontrado");
    }

    return result[1][0].dataValues;
  }
}

module.exports = ImovelService;
