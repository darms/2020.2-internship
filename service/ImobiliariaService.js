const imobiliaria = require("../models/imobiliaria");
const sequelize = require("../database/sequelize");
class ImobiliariaService {
  async createImobiliaria(data) {

    await imobiliaria.sync().then(function () {
      // Table created
      return imobiliaria
        .create({
          name: data.name,
          CNPJ: data.CNPJ,
        })
        .then((res) => {
          return res;
        })
    });

    return;
  }

  async getAllImobiliaria() {
    const result = await imobiliaria
      .findAll()
      .then((res)=>{
        return res;
      })
      .catch((error)=>{
        throw new Error('Não existe nenhuma Imobiliaria Cadastrada Ainda');
      });

    return result;
  }

  async getOneImobiliaria(cnpj){
    const result = await imobiliaria.findOne({
      where: {
        CNPJ: cnpj,
      },
    });
    if(result == null){
      throw new Error('Não foi Encontrado a imobiliaria');
    }
    return result;
  }


  async deleteImobiliaria(cnpj) {
    await imobiliaria.destroy({
      where: {
        CNPJ: cnpj,
      },
    });
    return;
  }

  async updateImobiliaria(data){

    const cnpj = data.newCNPJ || data.oldCNPJ;
    const imobiliariaName = data.newName;
    let validBody = 0;
    
    const dataUpdate = {};
    if(cnpj==data.newCNPJ){
      validBody=1;
      dataUpdate.CNPJ = cnpj;
    }
    if(imobiliariaName!=null){
      validBody=1;
      dataUpdate.name = imobiliariaName;
    }

    if(!validBody){
      var errorMessage =
        'O body da requisição está incorreto. Lembre-se de inserir no minímo oldCNPJ e (newCNPJ ou newName). Um exemplo correto de body { "oldCNPJ": "12345678910100","newCNPJ": "12345678910101","newName": "Casa12"}';
        throw new Error(errorMessage);
    }

    const result = await imobiliaria.update(
      dataUpdate,
      {
        returning: true,
        where: {
          CNPJ: data.oldCNPJ,
        },
      }
    );
    
    if (result[1][0] == undefined) {
      throw new Error("A imobiliaria não foi encontrada");
    }


    return result[1][0].dataValues;
  }
}

module.exports = ImobiliariaService;