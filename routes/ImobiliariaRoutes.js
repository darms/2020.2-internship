const express = require("express");
const ImobiliariaController = require('../controller/ImobiliariaController');

const imobiliaria = new ImobiliariaController();


const routes = express.Router();

routes.post("/createImobiliaria", async (req, res, next) => {
    imobiliaria.createImobiliaria(req,res);
});

routes.delete("/deleteImobiliaria/:cnpj", async (req, res) => {
  imobiliaria.deleteImobiliaria(req, res);
});

routes.get("/getAllImobiliaria", async (req, res) => {
  imobiliaria.getAllImobiliaria(req, res);
});

routes.get("/getOneImobiliaria/:cnpj", async (req, res) => {
  imobiliaria.getOneImobiliaria(req, res);
});

routes.put("/updateImobiliaria", async (req, res) => {
  imobiliaria.updateImobiliaria(req, res);
});




module.exports = routes;