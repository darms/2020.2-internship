const imobiliariaRoutes = require('./ImobiliariaRoutes');
const imovelRoutes = require('./ImovelRoutes');

module.exports = (app) => {
  app.use("/Imobiliaria", imobiliariaRoutes);
  app.use("/Imovel", imovelRoutes);
};
