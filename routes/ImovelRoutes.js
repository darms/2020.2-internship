const express = require("express");

const imovelController = require("../controller/ImovelController");

const imovel = new imovelController();

const routes = express.Router();

routes.post("/createImovel", async (req, res, next) => {
  imovel.createImovel(req, res);
});

routes.delete("/deleteImovel/:code", async (req, res) => {
  imovel.deleteImovel(req, res);
});

routes.get("/getAllImovel", async (req, res) => {
  imovel.getAllImovel(req, res);
});

routes.get("/getOneImovel/:code", async (req, res) => {
  imovel.getOneImovel(req, res);
});

routes.put("/updateImovel", async (req, res) => {
  imovel.updateImovel(req, res);
});


module.exports = routes;
