# Rua Dois - Estágio Backend

Bem-vindo(a) ao teste prático para estágio da RuaDois!

### Objetivo

Desenvolver uma API REST para gerenciar o cadastro de imobiliárias e imóveis.

### Requisitos

1. CRUD de imobiliárias
    1. A imobiliária deve possuir nome e CNPJ.
    1. Rotas para listar todas as imobiliárias, criar uma imobiliária, ver uma imobiliária, atualizar uma imobiliária e deletar uma imobiliária.
    1. O CNPJ da imobiliária deve ser único no sistema.
    1. Todos os atributos podem ser atualizados.
1. CRUD de imóveis
    1. O imóvel deve possuir código, tipo de imóvel, número de quartos, um status para indicar se o
       imóvel está publicado na plataforma, um status para indicar se o imóvel foi alugado e a
       informação sobre qual imobiliária ele pertence.
    1. Rotas para listar todos os imóveis, criar um imóvel, ver um imóvel, atualizar um imóvel e deletar um imóvel.
    1. O campo de código deve ser informado pelo usuário e pode ser alterado.
    1. O campo de código deve ser único no sistema.
    1. Os tipos de imóveis que a API pode aceitar são: apartamento, casa, galpão e sala comercial.
    1. Sempre que um imóvel for alugado, ele deve ser automaticamente despublicado no sistema.
    1. Antes de cadastrar o imóvel, deve-se verificar se a imobiliária a qual ele pertence existe no sistema.
    1. Não podemos alterar a imobiliária a qual o imóvel pertence.
    1. A rota de listagem dos imóveis deve poder filtrar os imóveis pelo número mínimo de quartos.

### Observações

A solução deve ser desenvolvida com base no framework **Node.js** e nas bibliotecas **express** e
**sequelize**. Não será avaliado se você tem conhecimento profundo das tecnologias, mas sim a sua
proposta de solução e a qualidade do código.

Nesse repositório, nós disponibilizamos a configuração inicial do servidor já integrado com o banco
de dados. Uma vez que você já tem o clone do projeto no seu local, basta executar o comando abaixo,
para subir o servidor:

`docker-compose up`

Para verificar se o servidor está ativo no seu computador e a configuração deu certo, basta acessar
`localhost:3000`. Se tudo estiver ok, o servidor irá retornar a seguinte mensagem `{"message":"Hello world!"}`.

Caso queira acessar o bash do container, basta executar a linha abaixo:

`docker-compose exec ruadois-api bash`

ou

`docker-compose run ruadois-api bash`

### Orientações

1. Criar um fork do repositório [https://gitlab.com/ruadois-public/2020.2-internship](https://gitlab.com/ruadois-public/2020.2-internship)
1. Desenvolver sua solução, e enviar o link do fork com a solução para **curriculodev@ruadois.com.br**
1. Enviar junto a solução um video curto de no máximo 5 minutos dando overview das suas decisões
   sobre a solução proposta.
1. Qualquer dúvida ou dificuldade referente a execução do teste, entrar em contato em
   **curriculodev@ruadois.com.br**
1. A solução deverá ser enviada até domingo (01/11/2020).

### Pontos importantes:
- Conhecimento sobre API REST;
- Arquitetura da solução;
- Tratamento de erros;
- Código limpo e boas práticas de programação.

Queremos ver seu processo de desenvolvimento, e para isso você pode utilizar o README para inserir outros pontos que considera importantes e que gostaria que avaliássemos com mais cuidado.

Happy hacking! ;)


## Notas de quem realizou o projeto
1. Existe um arquivo chamado Rua2Entrevista.postman_collection.json.
   1. Com esse arquivo é possivel importar a coleção que eu criei com o postman.
   2. Existem exemplos e detalhes de como usar cada rota.
2. Adicionar os pacotes cors e nodemon com yarn add "nome do pacote".
   1. Chamar yarn install.
