const { Sequelize} = require("sequelize");
const sequelize = require("../database/sequelize");

const imobiliaria = sequelize.define(
  "imobiliaria",
  {
    name: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      //field: "first_name", // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    CNPJ: {
      type: Sequelize.STRING,
      validate:{
                len: [14,14],
              },
      primaryKey: true,
    },
  },
  {
    freezeTableName: true, // Model tableName will be the same as the model name
  }
);

module.exports = imobiliaria;