const { Sequelize } = require("sequelize");
const sequelize = require("../database/sequelize");
const Imobiliaria = require("./imobiliaria");

const imovel = sequelize.define(
  "imovel",
  {
    code: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    category: {
      type: Sequelize.ENUM,
      values: ["apartamento", "casa", "galpão", "sala comercial"],
      allowNull: false,
    },
    roomQuantity: {
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    isRented: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    isPublished: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
  },
  {
    freezeTableName: true, // Model tableName will be the same as the model name
  }
);

imovel.belongsTo(Imobiliaria, {
  foreignKey: "imobiliariaCNPJ",
  as: "Imobiliaria",
  allowNull: false,
  onDelete: "restrict",
});

module.exports = imovel;
