const express = require('express');
const sequelize = require('./database/sequelize');
const setRoutes = require("./routes/BaseRoutes");
const cors = require("cors");


const app = express();

app.use(cors());
app.use(express.json());
setRoutes(app);

const port = 3000;
app.listen(port, () => {
  console.log(`Server running at ${port}`);
});


 